FROM python:3.6-buster
ADD . /
RUN cd /
RUN pip3 install cherrypy apsw Pillow
EXPOSE 8080/tcp

CMD ["python3","server.py"]
