import io
import apsw
import time
import draw_pic
import cherrypy
from secrets import token_urlsafe
from sys import platform, stdout, stderr
from os import path,getenv
from pyratemp import Template

class img:
    @cherrypy.expose
    def img(self, file):
        pth = "model/" + file
        rth = ""
        finded = False
        if(path.isfile(pth)):
            finded = True
            rth = pth
        pth += ".png"
        if(path.isfile(pth)):
            finded = True
            rth = pth
        if(finded):
            cherrypy.response.headers['Content-Type'] = 'image/png'
            with open(rth, "rb") as fp:
                img = fp.read()
            return img
        else:
            raise cherrypy.HTTPError(404, "404 Not Found")


class root(object):
    def __init__(self):
        self.staticmap = {"gitlab-logo.png":("gitlab-logo.png", "image/png"),"plurk.png":("plurk.png","image/png"),"facebook.png":("facebook.png","image/png"),"pinterest.png":("pinterest.png","image/png"),"tumblr.png":("tumblr.png","image/png"),"twitter.png":("twitter.png","image/png"),"twitter.png":("twitter.png","image/png"),"notoneriftc-regular.woff2":("notoneriftc-regular.woff2","font/woff2" )}
        self.wordrep = {"back_c": "#ffffff", "line_0": "", "line_0_c": "#993399", "line_1": "", "line_1_c": "#993399", "line_2": "", "line_2_c": "#993399", "sign": "", "sign_c": "#ff9900"}
        self.picmapt = {"linshit": 2, "how": 4, "UNCHR": 1, "Winne": 3, "ali": 0}
        self.otherdef = {"picture": "linshit", "mpicurl": "rlt/"}
        
        self.cnt = apsw.Connection("dbfile.db")
        self.init_sqlite()
        return

    def init_sqlite(self):
        qrt = "SELECT name FROM sqlite_master WHERE type='table' AND name='quote' COLLATE NOCASE;"
        cursor = self.cnt.cursor()
        rlt = cursor.execute(qrt)
        if(len(list(rlt)) == 0):
            qrt = "create table quote(uid INT NOT NULL PRIMARY KEY,secret TEXT,img NODE,expired FLOAT);"
            rlt = cursor.execute(qrt)
        cursor.close()
        return

    def sqlexec(self, qrt, bind=[], fone=True):
        cur = self.cnt.cursor()
        if(len(bind) == 0):
            cur.execute(qrt)
        else:
            cur.execute(qrt, bind)
        if(fone):
            rlt = cur.fetchone()
        else:
            rlt = cur.fetchall()
        cur.close()
        return rlt

    def handlecookie(self):
        expired = 60*60*6
        creatnew = False
        secret = ""
        uid = -1
        cie = cherrypy.request.cookie.keys()
        if("uid" in cie):
            uid = cherrypy.request.cookie["uid"].value
        if("secret" in cie):
            secret = cherrypy.request.cookie["secret"].value
        rlt = self.sqlexec("select uid,secret,img,expired from quote where uid = ?;", (uid,), True)
        if(None == rlt):
            creatnew = True
        else:
            self.sqlexec("update quote set expired = ? where uid = ?;",(time.time(),uid))
        if(creatnew):
            with open("model/test_pic2.png", "rb") as fp:
                img = fp.read()
            
            rmtar = self.sqlexec("select uid from quote where expired < ? order by expired DESC limit 5;", (time.time(),), False)
            cur = self.cnt.cursor()
            try:
                cur.executemany("delete from quote where uid = ?",rmtar)
            except apsw.SQLError:
                pass
            cur.close()
            
            uidls = self.sqlexec("select uid from quote order by uid ASC;", [], False)
            uidls = list(map(lambda x: x[0], uidls))
            if(len(uidls) == 0):
                uid = 1
            else:
                for i in range(1, max(uidls) + 1):
                    if(i in uidls):
                        pass
                    else:
                        uid = i
                        break
                else:
                    uid = max(uidls) + 1
            rlt = (uid, token_urlsafe(8), img, time.time() + expired)
            try:
                self.sqlexec("insert into quote values(?,?,?,?)", rlt)
            except apsw.SQLError:
                cur = self.cnt.cursor()
                cur.execute("BEGIN TRANSACTION;")
                uid = cur.execute("select MAX(uid) from quote").fetchone()
                if(uid[0] == None):
                    uid = 1
                else:
                    uid = int(uid[0]) + 1
                rlt = (uid, token_urlsafe(8), img, time.time() + expired)
                cur.execute("insert into quote values(?,?,?,?)", rlt)
                cur.execute("commit")
                cur.close()
            cherrypy.response.cookie["uid"] = rlt[0]
            cherrypy.response.cookie["uid"]["max-age"] = expired
            cherrypy.response.cookie["uid"]["path"] = "\\"
            cherrypy.response.cookie["secret"] = rlt[1]
            cherrypy.response.cookie["secret"]["max-age"] = expired
            cherrypy.response.cookie["secret"]["path"] = "\\"
        return rlt

    def readfilet(self, filename):
        with open(filename, "r", encoding="utf-8") as fp:
            f = fp.read()
        return f

    @cherrypy.expose
    def index(self):
        cie = self.handlecookie()
        std = self.wordrep.copy()
        std.update(self.otherdef.copy())
        std["picture"] = "linshit"
        std["mpicurl"] = "rlt/"
        return self.rander(std, self.picmapt[std["picture"]])

    @cherrypy.expose
    def mainjs(self):
        return self.readfilet("ugly_web.js")
    @cherrypy.expose
    def static(self, file):
        if file not in self.staticmap:
            raise cherrypy.HTTPError(404, "404 Not Found")
        fiinfo = self.staticmap[file]
        cherrypy.response.headers['Content-Type'] = fiinfo[1]
        with open("static/" + fiinfo[0], "rb") as fp:
            img = fp.read()
        return img
        
    @cherrypy.expose
    def quote(self, **know):
        """
        picture,back_c,line_0,line_0_c,line_1,line_1_c,line_2,line_2_c
        """
        content = []
        fp = io.BytesIO()
        pic_ptr = 0
        clkn = dict()
        cie = self.handlecookie()
        for i in self.wordrep:
            if(i in know):
                clkn[i] = know[i]
            else:
                clkn[i] = self.wordrep[i]
        for i in [("line_0", "line_0_c"), ("line_1", "line_1_c"), ("line_2", "line_2_c")]:
            if(i[0] in clkn and len(clkn[i[0]]) != 0 and i[1] in clkn):
                content.append((clkn[i[0]], clkn[i[1]]))
            else:
                break

        if(know["picture"] in self.picmapt):
            pic_ptr = self.picmapt[know["picture"]]

        draw_pic.quote(content, (clkn["sign"], clkn["sign_c"]), pic_ptr, clkn["back_c"]).save(fp, "PNG", compress_level=0)
        img = fp.getvalue()
        fp.close()
        self.sqlexec("update quote set img = ? where uid = ?", (img, cie[0]))
        return self.rander(clkn, pic_ptr)

    @cherrypy.expose
    def rlt(self, **know):
        cie = self.handlecookie()
        cherrypy.response.headers['Content-Type'] = 'image/png'
        return cie[2]

    @cherrypy.expose
    def djpg(self):
        cie = self.handlecookie()
        cherrypy.response.headers['Content-Type'] = 'image/JPEG'
        with io.BytesIO() as fp:
            fp.write(cie[2])
            with io.BytesIO() as rlt:
                draw_pic.openfp(fp).save(rlt, "JPEG", quality=100)
                bytearr = rlt.getvalue()
        return bytearr

    @cherrypy.expose
    def dpng(self):
        cie = self.handlecookie()
        cherrypy.response.headers['Content-Type'] = 'image/png'
        return cie[2]

    def rander(self, k, pic_ptr):
        picd = {"picture_" + str(j): "" for i, j in self.picmapt.items()}
        picd["picture_" + str(pic_ptr)] = "selected"
        picd.update(k)
        picd.update(self.otherdef.copy())
        t = Template(filename="ugly_rander.html")
        r = t(**picd)
        return r

    def _cp_dispatch(self, vpath):
        if(len(vpath) == 2 and vpath[0] == "img"):
            cherrypy.request.params['file'] = vpath.pop()
            return img()

port,host = int(getenv("SOCKET_PORT",default = 8080)), getenv("SOCKET_HOST",default = "127.0.0.1")
if(platform == "win32"):
    config = {"global": {'server.socket_port': port, "server.socket_host": host, 'log.access_file': 'access.log', 'log.error_file': 'error.log'}}
else:
    config = {"global": {'server.socket_port': port, "server.socket_host": host, 'log.access_file': 'access.log', 'log.error_file': 'error.log', "server.thread_pool": 6}}
if __name__ == '__main__':
    cherrypy.quickstart(root(),"/",config)
    """
    from cherrypy.process.plugins import Daemonizer
    cherrypy.config.update( {'server.socket_port': 80, "server.socket_host": "178.128.113.24","server.thread_pool" : 6,'log.access_file': 'access.log','log.error_file': 'error.log'})
    cherrypy.tree.mount(root, "/")
    server = Daemonizer(cherrypy.engine,stdout=stdout,stderr = stderr)
    server.subscribe()
    """

