from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


def quote(content, sign, pic_ptr, bcolor="blue"):
    lenth = len(content)
    if type(pic_ptr) == type(0):
        pic_ptr = str(pic_ptr)
    img = Image.open("model/spic" + pic_ptr + ".png")
    font = ImageFont.truetype(font="kaiu.ttf", size=70, encoding="unic")
    bg = Image.new(mode="RGBA", size=(img.size[0] * 2, img.size[1]), color=bcolor)
    r, g, b, a = img.split()
    bg.paste(img, (0, 0), mask=a)
    word = ImageDraw.Draw(bg)
    x = bg.size[0] // 13 * 6
    y = bg.size[1] // 9 * 2
    spa = bg.size[1] // 7
    for i in range(0, lenth):
        word.text((x, y + (spa * i)), content[i][0], fill=content[i][1], font=font)
        
    if len(sign) > 0:
        word.text((bg.size[0] // 19 * 12, bg.size[1] - y), sign[0], fill=sign[1], font=font)
    return bg


def openfp(fp):
    img = Image.open(fp)
    img = img.convert("RGB")
    return img


def gen_testpic():
    for i in range(0, 5):
        f = open("test_pic{}.png".format(i), "wb")
        q = quote([],[],i,"#ffffff")
        q.save(f, "png")
        f.close()
    return
# quote([b'\xe6\x88\x91\xe6\x98\xaf\xe6\xb8\xac\xe8\xa9\xa6\xe6\x96\x87\xe5\xad\x97'.decode("utf-8")],"--yvonne","0").show()
